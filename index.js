const express = require("express");
require("dotenv").config();
const Cors = require("cors");
const bodyParser = require("body-parser");
const db = require("./models");
const app = express();
const sequelize = db.sequelize;
const cors = { origin: "*", credentials: true };
app.use(Cors(cors));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.listen(process.env.PORT, () => {
  console.log(`server running on port ${process.env.PORT}`);
});

sequelize
    .authenticate()
    .then(() => {
        console.log("Connection has been established successfully.");
    })
    .catch((err) => {
        console.error("Unable to connect to the database:", err);
    });

const post = require("./routes/post.routes");
app.use("/post", post);

module.exports = app;
