module.exports = {
  up: (queryInterface, Sequelize) => {
    const postsData = [];

    for (let i = 0; i < 100; i++) {
      postsData.push({
        title: `Post ${i + 1}`,
        description: `Description ${i + 1}`,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    }

    return queryInterface.bulkInsert('posts', postsData);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('posts', null, {});
  }
};