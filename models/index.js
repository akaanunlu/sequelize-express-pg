const dbConfig = require("../config/config.js")
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.development.database, dbConfig.development.username,dbConfig.development.password, {
  host: dbConfig.development.host,
  dialect: dbConfig.development.dialect,
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.posts = require("./post.models.js")(sequelize, Sequelize);

module.exports = db;
